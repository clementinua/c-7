#include <iostream>

int main()
{
    setlocale(LC_ALL, "Russian");
    
    int n;
    std::cout << "Введите число: ";
    std::cin  >> n;
    
    unsigned int summary = n * (n + 1) / 2;
    
    std::cout << "Сумма чисел от 1 до " << n << " равна " << summary << std::endl;
    
    //system("pause");
    
    return 0; 
}
