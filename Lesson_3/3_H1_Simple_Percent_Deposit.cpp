#include <iostream>

int main()
{
    setlocale(LC_ALL, "Russian");
    
    // Процент, установленный банком.
    const float PERCENT = 0.07F;
    
    // Начальный клиентский вклад.
    int clientInputMoney = 0;
    std::cout << "Введите сумму вклада: ";
    std::cin  >> clientInputMoney;
    
    // Длительность вклада.
    int clientDuration = 0;
    std::cout << "Введите длительность: ";
    std::cin  >> clientDuration;
    
    // Расчет прибыли за месяц.
    int profitPerMonth = clientInputMoney * PERCENT;
    
    // Расчет общего тела депозита по прошествии указанного срока.
    int clientTotalMoney = profitPerMonth * clientDuration;
    
    // Расчет чистой прибыли клиента.
    int clientTotalProfit = clientTotalMoney - clientInputMoney;
    
    // Вывод данных на экран
    std::cout << std::endl;
    std::cout << "Тело депозита:  " << clientTotalMoney  << std::endl
              << "Чистая прибыль: " << clientTotalProfit << std::endl;
    
    //system("pause");
    
    return 0;
}
