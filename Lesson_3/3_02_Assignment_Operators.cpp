#include <iostream>

int main()
{
    
    float a = 10.0F,
          b = 0.0F;

    b += a;      // b = b + a       ->   0.0 + 10.0        =   10.0
    std::cout << b << std::endl;
    
    b -= a / 2;  // b = b - a / 2   ->   10.0 - 10.0 / 2   =   5.0
    std::cout << b << std::endl;
    
    b *= a;      // b = b * a       ->   5.0 * 10.0        =   50.0
    std::cout << b << std::endl;
    
    b /= a;      // b = b / a       ->   50.0 / 10.0       =   5.0
    std::cout << b << std::endl;
    
    //system("pause");
    
    return 0;
}
