#include <iostream>

int main()
{
    setlocale(LC_ALL, "Russian");
    
    const int THUMB_FINGER  = 1; // Большой
    const int FORE_FINGER   = 2; // Указательный
    const int MIDDLE_FINGER = 3; // Средний
    const int RING_FINGER   = 4; // Безымянный
    const int PINKY_FINGER  = 5; // Мизинец
    
    int finger = 0;
    std::cout << "Введите номер пальца руки: ";
    std::cin  >> finger;
    
    switch (finger) {
        case THUMB_FINGER: {
            std::cout << "Большой палец.";
            break;
        }
        case FORE_FINGER: {
            std::cout << "Указательный палец.";
            break;
        }
        case MIDDLE_FINGER: {
            std::cout << "Средний палец.";
            break;
        }
        case RING_FINGER: {
            std::cout << "Безымянный палец.";
            break;
        }
        case PINKY_FINGER: {
            std::cout << "Мизинец.";
            break;
        }
        default: std::cout << "Хм, судя по всему вы с другой планеты...";
    }
    
    std::cout << std::endl;
    
    return 0;
}
