#include <iostream>
#include <cmath>

int main()
{
    setlocale(LC_ALL, "Russian");
    
    // Коэффициенты квадратного уравнения.
    float a = 0.0F, 
          b = 0.0F, 
          c = 0.0F;
          
    std::cout << "Введите коэффициенты квадратного уравнения через пробел: " << std::endl;
    std::cin  >> a >> b >> c;
    
    // Вычисление дискриминанта
    //
    // Приведены два примера: первый демонстрирует вычисление квадрата числа
    // арифметическим способом, второй - с использованием функции возведения в степень.
    float d = b * b - 4 * a * c;
//  float d = std::pow(b, 2) - 4 * a * c;

    if ( d > 0.0F ) {
        float x1 = (-b + std::sqrt(d)) / (2 * a);
        float x2 = (-b - std::sqrt(d)) / (2 * a);
        std::cout << x1 << " " << x2 << std::endl;
    } else if ( d == 0.0F ) {
        float x = -b / 2 * a;
        std::cout << x << std::endl;
    } else {
        std::cout << "Корней нет или корни комплексные." << std::endl;
    }
    
    return 0;
}
