#include <iostream>

int main()
{
	setlocale(LC_ALL, "Russian");
	
	float first = 0.0F;
	std::cout << "Введите первое число: ";
	std::cin  >> first;
	
	char operation = '\0';
	std::cout << "Введите операцию (+, -, *, /): ";
	std::cin  >> operation;
	
	float second = 0.0F;
	std::cout << "Введите второе число: ";
	std::cin  >> second;
	
	float answer = 0.0F;
	
	switch (operation) {
		case '+': {
			answer = first + second;
			break;
		}
		case '-': {
			answer = first - second;
			break;
		}
		case '*': {
			answer = first * second;
			break;
		}
		case '/': {
			answer = first / second;
			break;
		}
		default: std::cout << "Операции не существует!";
	}
	
	std::cout << "Ответ: " << answer << std::endl;
	
	return 0;
}