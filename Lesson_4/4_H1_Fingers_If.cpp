#include <iostream>

int main()
{
    setlocale(LC_ALL, "Russian");
    
    const int THUMB_FINGER  = 1; // Большой
    const int FORE_FINGER   = 2; // Указательный
    const int MIDDLE_FINGER = 3; // Средний
    const int RING_FINGER   = 4; // Безымянный
    const int PINKY_FINGER  = 5; // Мизинец
    
    int finger = 0;
    std::cout << "Введите номер пальца руки: ";
    std::cin  >> finger;
    
    if ( THUMB_FINGER == finger ) {
        std::cout << "Большой палец.";
    } else if ( FORE_FINGER == finger ) {
        std::cout << "Указательный палец.";
    } else if ( MIDDLE_FINGER == finger ) {
        std::cout << "Средний палец.";
    } else if ( RING_FINGER == finger ) {
        std::cout << "Безымянный палец.";
    } else if ( PINKY_FINGER == finger ) {
        std::cout << "Мизинец.";
    } else {
        std::cout << "Хм, судя по всему вы с другой планеты...";
    }
    
    std::cout << std::endl;
    
    return 0;
}
