#include <iostream>

int main()
{
    setlocale(LC_ALL, "Russian");
    
    int number;
    std::cout << "Введите целое число: ";
    std::cin  >> number;
    
    if ( number % 2 == 0 ) {
        // Выполняется, если условие true
        std::cout << "Число четное!";
    } else {
        // Выполняется, если условие false
        std::cout << "Число нечетное!";
    }
    
    return 0;
}