#include <iostream>

int main()
{
    setlocale(LC_ALL, "Russian");
    
    int number;
    std::cout << "Введите целое число: ";
    std::cin  >> number;
    
    if ( number > 0 ) {
        // Выполняется, если первое условие true
        std::cout << "Число положительное!";
    } else if ( number < 0 ) {
        // Выполняется, если первое условие false, но второе true
        std::cout << "Число отрицательное!";
    } else {
        // Выполняется, если все условия false
        std::cout << "Ноль!";
    }
    
    std::cout << std::endl;
    
    return 0;
}