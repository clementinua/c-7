#include <iostream>

int main()
{
    setlocale(LC_ALL, "Russian");
    
    int number;
    std::cout << "Введите целое число: ";
    std::cin  >> number;
    
    std::cout << "Число " << (number % 2 == 0 ? "четное" : "нечетное") << "!" << std::endl;
    
    return 0;
}
