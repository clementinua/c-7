#include <iostream>
#include <iomanip>  // *1
#include <limits>   // *2, *3

int main()
{
    // Заголовок
    std::cout << std::setw(24) << "Type"
              << std::setw(24) << "Min"
              << std::setw(24) << "Max"
              << std::endl;

    // Boolean (bool)
    std::cout << std::setw(24) << "bool" 
              << std::setw(24) << std::numeric_limits<bool>::min()
              << std::setw(24) << std::numeric_limits<bool>::max()
              << std::endl;

    // Character (char)
    std::cout << std::setw(24) << "char" 
              << std::setw(24) << std::numeric_limits<char>::min()
              << std::setw(24) << std::numeric_limits<char>::max()
              << std::endl;
              
    // Wide Character (wchar_t) *4
    std::cout << std::setw(24) << "wchar_t" 
              << std::setw(24) << std::numeric_limits<wchar_t>::min()
              << std::setw(24) << std::numeric_limits<wchar_t>::max()
              << std::endl;

    // Integer (int)
    std::cout << std::setw(24) << "int" 
              << std::setw(24) << std::numeric_limits<int>::min()
              << std::setw(24) << std::numeric_limits<int>::max()
              << std::endl;

    // Long integer (long) *5
    std::cout << std::setw(24) << "long long" 
              << std::setw(24) << std::numeric_limits<long long>::min()
              << std::setw(24) << std::numeric_limits<long long>::max()
              << std::endl;

    // Unsigned long integer (unsigned long long)
    std::cout << std::setw(24) << "unsigned long long" 
              << std::setw(24) << std::numeric_limits<unsigned long long>::min()
              << std::setw(24) << std::numeric_limits<unsigned long long>::max()
              << std::endl;

    // Float (float)
    std::cout << std::setw(24) << "float" 
              << std::setw(24) << std::numeric_limits<float>::min()
              << std::setw(24) << std::numeric_limits<float>::max()
              << std::endl;

    // Double (double)
    std::cout << std::setw(24) << "double" 
              << std::setw(24) << std::numeric_limits<double>::min()
              << std::setw(24) << std::numeric_limits<double>::max()
              << std::endl;
    
    //system("pause");  // Для Visual Studio
    
    return 0;
}

/*
    *1. Функция std::setw объявлена в библиотеке <iomanip>
    *2. Документация по библиотеке <limits> для С++: http://www.cplusplus.com/reference/limits/
    *3. Документация конкретно по классу std::numeric_limits: http://www.cplusplus.com/reference/limits/numeric_limits/
    *4. wchar_t - символьный тип с большим диапазоном для хранения (С++11)
    *5. long long - целочисленный тип для хранения больших чисел (C++11)
*/
