#include <iostream>

int main()
{
    // Число Пи
    const float PI = 3.1415926F;
    
    // Радиус окружности
    float r = 15.0F;
    
    // Вычисление длины окружности
    float s = 2 * PI * r;
    
    std::cout << s << std::endl;
    
    //system("pause");  // Для Visual Studio
    
    return 0;
}
