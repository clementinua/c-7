#include <iostream>

int main()
{
    // Объявление и ициниализация переменной.
    int sqrt2 = 1.4142F;
    
    std::cout << "Square root of 2 is " << sqrt2 << std::endl;
    
    //system("pause");  // Для Visual Studio
    
    return 0;
}
