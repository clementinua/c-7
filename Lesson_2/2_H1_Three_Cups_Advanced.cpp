#include <iostream>

int main()
{
    setlocale(LC_ALL, "Russian");
    
    // Ввод данных
    int a = 0;
    std::cout << "Введите первое число: ";
    std::cin >> a;
    
    int b = 0;
    std::cout << "Введите второе число: ";
    std::cin >> b;
    
    // Вывод данных на экран
    std::cout << "a = " << a << ", b = " << b << std::endl;
    
    // Обмен значениями
    int temporary = a;
    a = b;
    b = temporary;

    // Вывод результата обмена
    std::cout << "a = " << a << ", b = " << b << std::endl;
    
    //system("pause");  // Для Visual Studio
    
    return 0;
}
