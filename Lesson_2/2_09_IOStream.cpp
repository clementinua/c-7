#include <iostream>

int main()
{
    // Подключение локали для отображение кириллицы в консоли.
    setlocale(LC_ALL, "Russian");
    
    std::cout << "Программа вычисления квадрата числа" << std::endl;
    std::cout << "Введите целое число: ";
    
    int number = 0;    // Обнуление переменных - хороший тон!
    std::cin >> number;
    
    int square = number * number;
    std::cout << "Квадрат числа " << number << " равен " << square << std::endl;
    
    //system("pause");  // Для Visual Studio
    
    return 0;
}
