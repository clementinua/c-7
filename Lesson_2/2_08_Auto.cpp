#include <iostream>

int main()
{
    auto i = 5;             // Автоматически выведет тип int
    auto f = 5.5F;          // Автоматически выведет тип float
    auto result = i * f;    // Выведет float (так как float имеет более высокий приоритет)
    
    std::cout << result << std::endl;
    
    //system("pause");  // Для Visual Studio
    
    return 0;
}
