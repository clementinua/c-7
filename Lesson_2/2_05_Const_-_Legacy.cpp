#include <iostream>

#define PI 3.1415926

int main()
{
    // Радиус окружности
    float r = 15.0F;
    
    // Вычисление длины окружности
    //
    // Поскольку число Пи имеет тип float и радиус окружности имеет тип float,
    // результат также должен быть сохранён в переменную типа float.
    float s = 2 * PI * r;
    
    std::cout << s << std::endl;
    
    //system("pause");  // Для Visual Studio
    
    return 0;
}
