#include <iostream>

int main()
{
    // Данные
    int a = 10, b = 50;
    
    // Вывод данных на экран
    std::cout << "a = " << a << ", b = " << b << std::endl;
    
    // Обмен значениями
    int temporary = a;
    a = b;
    b = temporary;

    // Вывод результата обмена
    std::cout << "a = " << a << ", b = " << b << std::endl;
    
    //system("pause");  // Для Visual Studio
    
    return 0;
}
