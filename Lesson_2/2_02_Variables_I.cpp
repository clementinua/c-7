#include <iostream>

int main()
{
    /*
        1. Объявление целочисленной переменной;
        2. Инициализация переменной числом (тип переменной
           и тип значения должны совпадать).
    */
    int integer;    // 1
    integer = 100;  // 2
    
    //integer = 100.5F; // Может привести к неопределённому поведению.
    
    std::cout << integer << std::endl;
    
    //system("pause");  // Для Visual Studio
    
    return 0;
}
