#include <iostream>

int main()
{
    5;                  // Целочисленный литерал.
    3.1415926F;         // Вещественный литерал.
    "This is literal!"; // Строковый литерал.

    // Вывод целочисленного литерала на экран.
    std::cout << 10 << std::endl;

    // Вывод суммы целочисленных литералов.
    std::cout << 15 + 25 << std::endl;

    // Вывод стровокого литерала.
    std::cout << "Hello, ORT!" << std::endl;

    //system("pause");  // Для Visual Studio

    return 0;
}
