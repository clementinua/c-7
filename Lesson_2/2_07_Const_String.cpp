#include <iostream>

int main()
{
    // Правильное представление константной строки:
    // константный указатель* на константное значение.
    const char* const STRING = "Hello, ORT";
    
    std::cout << STRING << std::endl;
    
    return 0;
}

// *Указатели будут разбираться намного позже.
